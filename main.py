import numpy as np
import pygame
import sys
import random
from random import randrange

import Settings
VARS = Settings.VARS()
CONST = Settings.CONST()

import Map
from Map import *

import Player
from Player import Player

def main():
	global screen, mapGrid, enemyGrid, enemyCollisionGrid, tileSize, clock
	clock = pygame.time.Clock()

	# Create a black screen in preparation for rendering tiles
	pygame.init()
	VARS.screen_size = (CONST.MAP_WIDTH * CONST.TILE_SIZE, CONST.MAP_HEIGHT * CONST.TILE_SIZE) # Screen dimentions
	VARS.screen = pygame.display.set_mode(VARS.screen_size)
	VARS.screen.fill((0, 0, 0))

	# Generating the map and enemy grids
	VARS.map_grid = generateMapGrid(CONST.MAP_WIDTH, CONST.MAP_HEIGHT) # 2d array
	
	VARS.enemy_grid = np.empty((CONST.MAP_HEIGHT, CONST.MAP_WIDTH), dtype='str')
	VARS.enemy_grid[:] = ' '

	VARS.enemy_col_grid = np.empty((CONST.MAP_HEIGHT, CONST.MAP_WIDTH), dtype='str')
	VARS.enemy_col_grid[:] = ' '

	# Generating player on a random land tile
	player_pos = getFreeLand(VARS, CONST)
	player = Player(player_pos, CONST)

	# Generate the enemies on random land tiles
	generateEnemies(CONST.NO_ENEMIES, 0, VARS, CONST)

	while True:
		# Detects exit of game
		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				pygame.quit()
				sys.exit()

		# Render the map
		drawMapGrid(VARS, CONST)
		
		# Render each enemy
		for enemy in VARS.enemies:
			enemy.render(VARS, CONST)

		# Update the position of the player (movements controlled here)
		player.update(VARS, CONST)

		# Render the player
		player.render(VARS, CONST)

		pygame.display.update()
		VARS.clock.tick(CONST.FPS) 
	
main()
