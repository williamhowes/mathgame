import pygame
import sys
from random import randrange

import mathsQuestions
from mathsQuestions import questions

import Map
from Map import generateEnemies

active = False
text = ''

player_dir = 'assets/player.png' # Relative path from current working dir
enemy_dir = 'assets/enemy.png' # Relative path from current working dir
font_dir = 'assets/freesansbold.ttf'

allowed_characters = '1234567890' # Characters allowed in answer input

combat_screen_running = False
game_over = False
question_active = False
display_dmg = False
dmg_dialogue = {}

def createCombatScreen(player, enemy, VARS, CONST):
    global combat_screen_running
    combat_screen_running = True

    VARS.screen= pygame.display.set_mode(VARS.screen_size)
    clock = pygame.time.Clock()
    answer = ''

    reg_font = pygame.font.Font(font_dir, 25)
    small_font = pygame.font.Font(font_dir, 20)

    while combat_screen_running:
        # Events triggered by keypresses or mouse clicks
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            if event.type == pygame.MOUSEBUTTONDOWN:
                if (question_active == False):
                    askQuestion(enemy)
            if event.type == pygame.KEYDOWN:
                if question_active:
                    if (event.key == pygame.K_RETURN and answer != ''):
                        answerQuestion(answer, player, enemy, VARS, CONST)
                        answer = ''
                    elif event.key == pygame.K_BACKSPACE:
                        answer = answer[:-1]
                    else:
                        if (len(answer) < 12):
                            if event.unicode in allowed_characters: 
                                answer += event.unicode

        # Fill the background with black
        VARS.screen.fill((0, 0, 0))

        # No question has been asked, render the player, enemy and text
        if (question_active == False and game_over == False):
            # Render the player and enemy images
            playerImage = pygame.image.load(player_dir).convert()
            VARS.screen.blit(playerImage, (100, VARS.screen_size[1]-200))

            enemyImage = pygame.image.load(enemy_dir).convert()
            VARS.screen.blit(enemyImage, (VARS.screen_size[0]-170, 70))

            # Render the difficulty and attack text
            color = (255,255,255)
            difficultyText = small_font.render('Enemy difficulty: ' + str(enemy.difficulty), True, color)
            attackText = reg_font.render('Click to attack!', True, color)
            playerHealth = reg_font.render("Player HP: " + str(player.health), True, color)
            enemyHealth = reg_font.render("Enemy HP: " + str(enemy.health), True, color)

            VARS.screen.blit(difficultyText, (40,50))
            VARS.screen.blit(attackText, (40,90))
            VARS.screen.blit(playerHealth, (50, VARS.screen_size[1]-70))
            VARS.screen.blit(enemyHealth, (VARS.screen_size[0]-220, 200))

            # Display damage dealt
            if display_dmg:
                color = (255,0,0)

                # ____ has dealt ____ damage to ____
                dialogue = dmg_dialogue['dealer'] + ' has dealt ' + dmg_dialogue['damage_dealt'] + ' damage to ' + dmg_dialogue['reciever']

                damage = small_font.render(dialogue, True, color)
                VARS.screen.blit(damage, (VARS.screen_size[0]/2, VARS.screen_size[1]-60))
        
        # Question has been asked, render active question
        if (question_active == True and game_over == False):
            color = (255,255,255)
            text = reg_font.render('What is ' + question['question'] + '?', True, color)
            text_size = text.get_rect() # Gets width and height of text object
            text_x = VARS.screen_size[0]/2 - (text_size.width/2)
            text_y = VARS.screen_size[1]/2 - (text_size.height/2) - 100
            VARS.screen.blit(text, (text_x, text_y))

            answer_prompt = small_font.render('Answer:', True, color)
            VARS.screen.blit(answer_prompt, (VARS.screen_size[0]/2 - 100, VARS.screen_size[1]/2 - 50))

            answer_rect = pygame.Rect(VARS.screen_size[0]/2 - 100, VARS.screen_size[1]/2 - 20, 200, 45)
            pygame.draw.rect(VARS.screen, (50, 50, 50), answer_rect)

            answer_text = reg_font.render(answer, True, color)
            VARS.screen.blit(answer_text, (VARS.screen_size[0]/2 - 90, VARS.screen_size[1]/2 - 10))

        # Game over - display player's score
        if game_over:
            color = (255,255,255)
            text = reg_font.render('Your final score is ' + str(player.score), True, color)
            text_size = text.get_rect() # Gets width and height of text object
            text_x = VARS.screen_size[0]/2 - (text_size.width/2)
            text_y = VARS.screen_size[1]/2 - (text_size.height/2)
            VARS.screen.blit(text, (text_x, text_y))

        pygame.display.update()
        clock.tick(2) 

def askQuestion(enemy):
    global question, question_active, selecting_question

    selecting_question = True
    while selecting_question:
        index = randrange(len(questions))
        question = questions[index]
        if (question['difficulty'] == enemy.difficulty):
            selecting_question = False

    question_active = True

def answerQuestion(submitted_answer, player, enemy, VARS, CONST):
    global question_active, dmg_dialogue, display_dmg

    # Gets the valid answer from the currently active question
    valid_answer = question['answer']

    if (submitted_answer == valid_answer):
        # Player is correct
        enemy.health -= player.damage
        dmg_dialogue = {
            'dealer': 'Player',
            'reciever': 'Enemy',
            'damage_dealt': str(player.damage)
        }
    else:
        # Player is wrong
        player.health -= enemy.damage

        dmg_dialogue = {
            'dealer': 'Enemy',
            'reciever': 'Player',
            'damage_dealt': str(enemy.damage)
        }

    if player.health == 0:
        playerDefeated(player, enemy)
    elif enemy.health == 0:
        enemyDefeated(player, enemy, VARS, CONST)

    # Halt rendering of the question popup, go back to rendering enemy and player
    display_dmg = True
    question_active = False
  
def playerDefeated(player, enemy):
    global game_over
    # This stops all rendering on the combat VARS.screenand insteads renders players score
    game_over = True

def enemyDefeated(player, enemy, VARS, CONST):
    global combat_screen_running
    # Points formula
    points = int((enemy.difficulty * 25) * (player.health/100))
    player.score += points

    # Exit out of the combat screen
    combat_screen_running = False
    difficulty = enemy.difficulty + 1
    generateEnemies(1, difficulty, VARS, CONST)
    






