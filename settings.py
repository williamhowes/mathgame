import pygame

# Constants that affect gameplay
class CONST():
	def __init__(self):
		self.FPS = 5 # Game FPS
		self.MAP_WIDTH = 64 # Map width in tiles
		self.MAP_HEIGHT = 32 # Map height in tiles
		self.TILE_SIZE = 16 # Size of each tile in pixels
		self.LAND_THRESH = -0.2 # Land threshold
		self.NO_ENEMIES = 10 # Number of enemies 

		self.PLAYER_HEALTH = 100 # Player starting health
		self.PLAYER_DMG = 50 # Player damage
		self.ENEMY_HEALTH = 100 # Enemy starting health
		self.ENEMY_DMG = 50 # Enemy damage

# Game variables
class VARS():
	def __init__(self):
		self.enemies = [] # Stores all enemy objects
		self.enemy_grid = [] # Stores enemy positions
		self.enemy_col_grid = [] # Stores enemy collision tiles
		self.enemy_count = 0 # Used to give each enemy a unique ID
		self.map_grid = [] # Stores all tile data
		self.clock = pygame.time.Clock() # Used for timing
		self.screen_size = None
		self.screen = None