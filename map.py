# Dependencies
import pygame
import perlin_numpy
from perlin_numpy import generate_perlin_noise_2d
import random
from random import randrange

import Enemy
from Enemy import Enemy

# Generate a 2D grid and fill with floats from -1 to 1 using perlin noise
def generateMapGrid(width, height):
	grid = generate_perlin_noise_2d((height, width), (2, 2)) # fills array with values from -1 to 1
	return grid # returns 2d array containing vals from -1 to 1 (floats)

# For each tile, render its position and colour on the map
def drawMapGrid(VARS, CONST):
	# Nested for loops to cycle through every tile
	for y in range(CONST.MAP_HEIGHT): 
		for x in range(CONST.MAP_WIDTH):

			rect = pygame.Rect(x*CONST.TILE_SIZE, y*CONST.TILE_SIZE, CONST.TILE_SIZE, CONST.TILE_SIZE)

			noise_level = VARS.map_grid[y][x] # From -1 to 1
			noise_level = int(round(noise_level*10)) # From -10 to 10, and rounded to an integer
		
			# By default, the tile is blue (water)
			col = {
				'r': 0,
				'g': 30,
				'b': 240
			}

			# If the noise at this position is >= the land threshold, 
			# ...the tile is made a variable shade of green
			if (VARS.map_grid[y][x] >= CONST.LAND_THRESH):
				col = {
					'r': 0,
					'g': 255 - 8*(10-noise_level),
					'b': 50
				}
				
			pygame.draw.rect(VARS.screen, (col['r'], col['g'], col['b']), rect)

			# Uncommenting below code will diplay collision tiles

			# if (enemyCollisionGrid[y][x] != ' '):
			# 	pygame.draw.rect(screen, (255, 0, 0), rect)

# Generate n enemies with random position and direction
def generateEnemies(n, d, VARS, CONST):
	for i in range(n): 
		location_set = False

		position = getFreeLand(VARS, CONST)
		direction = randrange(4) # Either 0, 1, 2, 3 (NSEW)
		difficulty = d # Enemy difficulty
		if (d == 0):
			difficulty = (randrange(2) + 1)

		enemy = Enemy(position, direction, VARS.enemy_count, difficulty, CONST)
		VARS.enemies.append(enemy)

		VARS.enemy_grid[position[1]][position[0]] = '#'
		VARS.enemy_col_grid[position[1]][position[0]] = VARS.enemy_count;

		for shift in range(1, 4): # Generate 3 tiles out from origin
			if (direction == 0): # North
				x = position[0]
				y = position[1] - shift
				if (y >= 0):
					# Generate the tile
					VARS.enemy_col_grid[y][x] = VARS.enemy_count

			if (direction == 1): # East
				x = position[0] + shift
				y = position[1]
				if (x <= CONST.MAP_WIDTH-1):
					# Generate the tile
					VARS.enemy_col_grid[y][x] = VARS.enemy_count

			if (direction == 2): # South
				x = position[0] 
				y = position[1] + shift
				if (y <= CONST.MAP_HEIGHT-1):
					# Generate the tile
					VARS.enemy_col_grid[y][x] = VARS.enemy_count

			if (direction == 3): # West
				x = position[0] - shift
				y = position[1]
				if (x >= 0):
					# Generate the tile
					VARS.enemy_col_grid[y][x] = VARS.enemy_count

		# Change enemy number so every enemy has a unique ID
		VARS.enemy_count += 1

# Returns the position of a random land tile
def getFreeLand(VARS, CONST):
	position = [0, 0]
	
	while True:
		y = int(random.random() * CONST.MAP_HEIGHT)
		x = int(random.random() * CONST.MAP_WIDTH)
		
		land = VARS.map_grid[y][x] >= CONST.LAND_THRESH
		enemy = VARS.enemy_grid[y][x] == '#'
		collision_path = VARS.enemy_col_grid[y][x] != ' '

		if (land == True and enemy == False and collision_path == False): # Check map to see if tile is grass
			position = [x, y]
			break

	return position