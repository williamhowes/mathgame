import numpy as np
import pygame
import combatScreen
from combatScreen import createCombatScreen

font_dir = 'assets/freesansbold.ttf'

class Player():
	def __init__(self, position, CONST):
		self.position = position
		self.direction = 3
		self.health = CONST.PLAYER_HEALTH
		self.damage = CONST.PLAYER_DMG
		self.score = 0
		self.last_pos = [self.position[0], self.position[1]]
		self.reg_font = pygame.font.Font(font_dir, 25)

	# The update method allows the Player to set key information about itself.
	# For example, it can detect if a key is prssed and update the position accordingly.
	def update(self, VARS, CONST):
		# Detecting keypresses and translating into movement
		# This only runs as fast as the refresh rate of pygame, set in Main.py
		keys = pygame.key.get_pressed()

		if (keys[pygame.K_w] and self.position[1] > 0):
			self.position[1] -= 1
			self.direction = 0

		if (keys[pygame.K_s] and self.position[1] < CONST.MAP_HEIGHT-1):
			self.position[1] += 1
			self.direction = 2

		if (keys[pygame.K_a] and self.position[0] > 0):
			self.position[0] -= 1
			self.direction = 3

		if (keys[pygame.K_d] and self.position[0] < CONST.MAP_WIDTH-1):
			self.position[0] += 1
			self.direction = 1

		x = self.position[0]
		y = self.position[1]

		if (VARS.map_grid[y][x] < CONST.LAND_THRESH): # Moved onto water
			self.position = [self.last_pos[0], self.last_pos[1]]
		else:
			self.last_pos = [x, y]

		# Test for enemy collisions
		x = self.position[0]
		y = self.position[1]

		# Get the data for the tile the played upon
		enemy_id = VARS.enemy_col_grid[y][x]

		if (enemy_id != ' '):
			# Player has collided
			height = len(VARS.enemy_col_grid)
			width = len(VARS.enemy_col_grid[0])

			# For each tile in the enemyCollisionGrid, check if the number matches
			for y in range(height): 
				for x in range(width):
					if (VARS.enemy_col_grid[y][x] == enemy_id):
						VARS.enemy_col_grid[y][x] = ' '

			enemy = VARS.enemies[0]
			# Search through enemies until matching ID has been found
			for instance in VARS.enemies:
				if (instance.unique_id == enemy_id):
					enemy = instance

			createCombatScreen(self, enemy, VARS, CONST)

			# Removes enemy from enemy array and removes enemy position from VARS.enemy_grid
			VARS.enemies.remove(enemy)
			enemy_x = enemy.position[0]
			enemy_y = enemy.position[1]
			VARS.enemy_grid[enemy_y][enemy_x] = ' '

			return enemy
		return False

	# Each Player renders itself using its own position and direction
	def render(self, VARS, CONST):
		# Here each enemy renders itself. It can do so differently depending on its own properties, e.g. direction.
		oX = self.position[0]*CONST.TILE_SIZE
		oY = self.position[1]*CONST.TILE_SIZE

		if (self.direction == 0): # N
			p1 = [ oX + CONST.TILE_SIZE/2, oY ] # top point
			p2 = [ oX, oY + CONST.TILE_SIZE ] # left point
			p3 = [ oX + CONST.TILE_SIZE, oY + CONST.TILE_SIZE ] # right point
			coordinates = [p1, p2, p3]

			pygame.draw.polygon(VARS.screen, (255, 0, 0), coordinates)

		if (self.direction == 1): # E
			p1 = [ oX + CONST.TILE_SIZE, oY + CONST.TILE_SIZE/2 ] 
			p2 = [ oX, oY ]
			p3 = [ oX , oY + CONST.TILE_SIZE ]
			coordinates = [p1, p2, p3]

			pygame.draw.polygon(VARS.screen, (255, 0, 0), coordinates)

		if (self.direction == 2): # S
			p1 = [ oX, oY ] 
			p2 = [ oX + CONST.TILE_SIZE, oY ]
			p3 = [ oX + CONST.TILE_SIZE/2, oY + CONST.TILE_SIZE ] 
			coordinates = [p1, p2, p3]

			pygame.draw.polygon(VARS.screen, (255, 0, 0), coordinates)

		if (self.direction == 3): # W
			p1 = [ oX , oY + CONST.TILE_SIZE/2 ] 
			p2 = [ oX + CONST.TILE_SIZE, oY ]
			p3 = [ oX + CONST.TILE_SIZE , oY + CONST.TILE_SIZE ]
			coordinates = [p1, p2, p3]

			pygame.draw.polygon(VARS.screen, (255, 0, 0), coordinates)

		# Render the score onto the VARS.screen
		color = (255,255,255)
		text = self.reg_font.render('Score: ' + str(self.score), True, color)
		VARS.screen.blit(text, (20, 20))

