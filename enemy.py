import pygame

colours = {
	1: (59, 65, 255),
	2: (225, 19, 232),
	3: (255, 242, 56),
	4: (255, 134, 36),
	5: (255, 20, 20)
}

class Enemy():
	def __init__(self, position, direction, unique_id, difficulty, CONST):
		self.position = position
		self.direction = direction
		self.unique_id = str(unique_id)
		self.health = CONST.ENEMY_HEALTH
		self.damage = CONST.ENEMY_DMG
		self.difficulty = difficulty

	# Each Enemy renders itself using its own position and direction
	def render(self, VARS, CONST):
		# Set the origin to be rendered from
		oX = self.position[0]*CONST.TILE_SIZE
		oY = self.position[1]*CONST.TILE_SIZE

		colour = colours[self.difficulty]

		if (self.direction == 0): # N
			p1 = [ oX + CONST.TILE_SIZE/2, oY ] # top point
			p2 = [ oX, oY + CONST.TILE_SIZE ] # left point
			p3 = [ oX + CONST.TILE_SIZE, oY + CONST.TILE_SIZE ] # right point
			coordinates = [p1, p2, p3]

			pygame.draw.polygon(VARS.screen, colour, coordinates)

		if (self.direction == 1): # E
			p1 = [ oX + CONST.TILE_SIZE, oY + CONST.TILE_SIZE/2 ] 
			p2 = [ oX, oY ]
			p3 = [ oX , oY + CONST.TILE_SIZE ]
			coordinates = [p1, p2, p3]

			pygame.draw.polygon(VARS.screen, colour, coordinates)

		if (self.direction == 2): # S
			p1 = [ oX, oY ] 
			p2 = [ oX + CONST.TILE_SIZE, oY ]
			p3 = [ oX + CONST.TILE_SIZE/2, oY + CONST.TILE_SIZE ] 
			coordinates = [p1, p2, p3]

			pygame.draw.polygon(VARS.screen, colour, coordinates)

		if (self.direction == 3): # W
			p1 = [ oX , oY + CONST.TILE_SIZE/2 ] 
			p2 = [ oX + CONST.TILE_SIZE, oY ]
			p3 = [ oX + CONST.TILE_SIZE , oY + CONST.TILE_SIZE ]
			coordinates = [p1, p2, p3]

			pygame.draw.polygon(VARS.screen, colour, coordinates)